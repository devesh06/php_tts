<?php
date_default_timezone_set('Asia/Kolkata');
require __DIR__ . '/vendor/autoload.php';

require_once __DIR__.'/assign_variables.php';
require_once __DIR__.'/db.php';

use Google\Cloud\TextToSpeech\V1\AudioConfig;
use Google\Cloud\TextToSpeech\V1\AudioEncoding;
use Google\Cloud\TextToSpeech\V1\SsmlVoiceGender;
use Google\Cloud\TextToSpeech\V1\SynthesisInput;
use Google\Cloud\TextToSpeech\V1\TextToSpeechClient;
use Google\Cloud\TextToSpeech\V1\VoiceSelectionParams;

class TextToSpeech
{
    public function __construct()
    {
        $this->db_obj = new Db();

        $this->google_key = '/var/config/tts/google_key.json';
        $this->format = 'mp3';
        $this->dir = $this->db_obj->config['dir'];

        $this->validate_key();

        if (!file_exists($this->google_key)) {
            die("The google_key file is not present! Make sure filename is correct.");
        }

        if (!isset($_GET['text'])) {
            die("Please provide the text field");
        }

        if (!isset($_GET['file'])) {
            die("Please provide the file name");
        }

        $this->make_log();
        // $this->check_limit();
        $this->set_request_varaibles();
    }

    function set_request_varaibles()
    {
        if (isset($_GET['filetype'])) {
            $file_format = strtolower($_GET['filetype']);
            $file_format = str_replace(str_split('\\/:*?"<>|-_ '),'', $file_format);
            if (($file_format == 'mp3') || $file_format == 'wav') {
                $this->format = $file_format;
            }
        }

        $this->filename = $_GET['file'];
        if (strpos($this->filename, ".wav") === false && strpos($this->filename, ".mp3") === false) {
            $this->filename =  $this->filename.'.'.$this->format;
        }
        $this->is_cached = 1;
        $this->text = $_GET['text'];
        $this->voice = get_gender_voice();
        $this->language = get_language();
        $this->volume = get_volume();
        $this->pitch = get_pitch($this->voice);
        $this->speed = get_speed($this->voice);
        $this->extension = substr($this->filename, -4);
        $this->wavenet = "";
        if(isset($_GET['wavenet'])) {
            $this->wavenet = trim($_GET['wavenet']);
        }


        $this->file_hash_name = md5($this->text . $this->voice . $this->language . $this->volume. $this->pitch. $this->speed. $this->extension . $this->wavenet) . $this->extension;

        $this->file = $this->dir . $this->file_hash_name;

        // echo "text: " . $this->text. " file:" . $this->file . " filename:". $this->filename ." voice:" . $this->voice . " language:" . $this->language . " volume:" . $this->volume . " pitch:" . $this->pitch . " speed:" . $this->speed;die;
    }

    public function generate_audio()
    {

        if ($this->voice == 'MALE') {
            //name is Wavenet-B for all male voices
            $name = $this->language.'-Wavenet-B';
            $gender_voice = SsmlVoiceGender::MALE;
        } elseif ($this->voice == 'FEMALE' && $this->language == 'en-US') {
            //for en-US FEMALE voice name should use Wavenet-C
            $name= $this->language.'-Wavenet-C';
            $gender_voice = SsmlVoiceGender::FEMALE;
        } elseif ($this->voice == 'FEMALE') {
            //for all other female voices name is Wavenet-A
            $name = $this->language.'-Wavenet-A';
            $gender_voice = SsmlVoiceGender::FEMALE;
        } else {
            die('There was error in getting the gender voice');
        }

        if(isset($_GET['voice']) && isset($_GET['language']) && isset($_GET['wavenet'])) {
            $name = trim($_GET['wavenet']);
            $gender_voice = SsmlVoiceGender::FEMALE;
            if ($this->voice == 'MALE') {
                $gender_voice = SsmlVoiceGender::MALE;
            }
        }

        try {
            $synthesisInputText = (new SynthesisInput())->setText($this->text);

            $voice = (new VoiceSelectionParams())
                ->setName($name)
                ->setSsmlGender($gender_voice)
                ->setLanguageCode($this->language);

            $audioConfig = (new AudioConfig())
                ->setAudioEncoding(AudioEncoding::MP3)
                ->setPitch($this->pitch)
                ->setSpeakingRate($this->speed)
                ->setVolumeGainDb($this->volume)
                ->setEffectsProfileId(array(get_effectsProfileId()));

            $this->client = new TextToSpeechClient([
                'credentials' => ($this->google_key)
            ]);

            $response = $this->client->synthesizeSpeech($synthesisInputText, $voice, $audioConfig);
            $audioContent = $response->getAudioContent();
            file_put_contents($this->file,$audioContent);

            return true;
        } catch(Exception $e) {
            echo "Error in generating audio:".$e->getMessage();
        }
    }

    public function download_audio_file()
    {
        if (!file_exists($this->file)) {
            $this->generate_audio();
            $this->is_cached = 0;
        }
        $this->insert_variables();
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename='.$this->filename);
        header('Content-Type: audio/mpeg;audio/wav'); // content type is for both .wav and .mp3
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: no-cache');
        header('Pragma: public');
        header('Content-Length:' .filesize($this->file));
        readfile($this->file);
        exit();
    }

    public function validate_key()
    {
        $user_key = @$_GET['key'];
        if (!isset($user_key)) {
            die('Key parameter is required!');
        }

        $permanent_key = md5($this->db_obj->config['key']);
        if ($user_key === $permanent_key) {
            return true;
        }

        $salt = $this->db_obj->config['salt'];
        $valid_keys = [];
        for ($i = 1; $i  <= $this->db_obj->config['key_valid_days']; $i++) {
            array_push($valid_keys, md5($salt . date('y-m-d', strtotime("-$i days"))));
        }

        if (!in_array($user_key, $valid_keys)) {
            $this->make_log('Key is expired');
            die('Key is expired!');
        }
        return true;
    }

    public function check_limit()
    {
        $dailyLimit = $this->db_obj->config['daily_limit'];
        $counter = 0;
        $date = date("Y-m-d");

        $get_counter_query = "select count(*) as count from request where date='$date'";
        $content_parts = $this->db_obj->execute_query($get_counter_query);
        $counter = $content_parts['count'];

        if ($counter >= $dailyLimit) {
            die("Daily executing limit exceeded! Please try again tomorrow.");
        }
        return true;
    }

    public function make_log($msg=null)
    {
        $log_file = $this->dir."logs.txt";

        if (is_null($msg)) {
            $log_msg = "IP: ".$_SERVER['REMOTE_ADDR'].'; DATE: '.date('Y-m-d H:i:s').'; TEXT: '.$_GET['text']. '; FILENAME: '. $_GET['file'] . ' Key: ' . $_GET['key'].PHP_EOL.'--------------------------------------'.PHP_EOL;
        } else {
            $log_msg = "Error: ". $msg ." IP: ".$_SERVER['REMOTE_ADDR'].'; DATE: '.date('Y-m-d H:i:s').'; TEXT: '.$_GET['text']. ' Key: ' . $_GET['key'].PHP_EOL.'--------------------------------------'.PHP_EOL;
        }
        file_put_contents($log_file, $log_msg, FILE_APPEND);
    }

    public function insert_variables()
    {
        $date = date("Y-m-d");
        $ip_address = $_SERVER['REMOTE_ADDR'];
        $tts_key = $_GET['key'];
        $text = $this->text;
        $character_count = strlen($this->text);
        $language = $this->language;
        $voice = $this->voice;
        $pitch = $this->pitch;
        $speed = $this->speed;
        $filename = $this->filename;
        $is_cached = $this->is_cached;

        $query = "insert into request(date,ip_address,tts_key,text,character_count,language,voice,pitch,speed,filename, is_cached) values('$date','$ip_address','$tts_key','$text','$character_count','$language','$voice','$pitch','$speed','$filename', '$is_cached')";

        $this->db_obj->execute_query($query);

        return true;
    }
}

$tts = new TextToSpeech();
$tts->download_audio_file();

?>
