FROM php:5.6-apache

WORKDIR /var/www/html

VOLUME ["/var/www/html"]

RUN mkdir -p /var/config/tts/
RUN mkdir -p /var/log/app/tts/
RUN mkdir -p /var/tts/
RUN chmod 777 -R /var/log/app/tts/
RUN chmod 777 -R /var/tts

RUN touch /var/config/tts/config.json
RUN touch /var/config/tts/google_key.json
RUN touch /var/tts/logs.txt

RUN docker-php-ext-install mbstring pdo pdo_mysql mysql mysqli

RUN apt-get update && apt-get install -y \
    vim \
    memcached \
    libz-dev libmemcached-dev

RUN pecl install memcached-2.2.0

RUN echo extension=memcached.so >> /usr/local/etc/php/conf.d/memcached.ini