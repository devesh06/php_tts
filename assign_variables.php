<?php

function get_pitch($voice)
{
	if (isset($_GET['pitch'])) {
        if (($_GET['pitch'] >= -20) && ($_GET['pitch'] <= 20)) {
            $pitch = $_GET['pitch'];
        } else {
            die("Please use value between -20 and 20 of 'pitch'.");
        }
    } else {
		if ($voice == 'MALE') {
			$pitch = 0;
		} else {
			$pitch = -2.8;
		}
    }
    return $pitch;
}

function get_speed($voice)
{
	if (isset($_GET['speed'])) {
        if (($_GET['speed'] >= 0.25) && ($_GET['speed'] <= 4.0 )) {
            $speaking_rate = $_GET['speed'];
        } else {
            die("Please use value between 0.25 and 4.0 or check if name is correct 'speed' ");
        }
    } else {
		if ($voice == 'MALE') {
			$speaking_rate = 0.8;
		} else {
			$speaking_rate = 0.9;
		}
    }
    return $speaking_rate;
}

function get_volume()
{
	if (isset($_GET['volume'])) {
        if (($_GET['volume'] >= -96.0) && ($_GET['volume'] <= 16.0 )) {
            $volume_gain = $_GET['volume'];
        } else {
            die("Please use value between -96.0 and 16.0 or check if name is correct 'volume' ");
        }
    } else {
        $volume_gain = 0.0;
    }
    return $volume_gain;
}

function get_effectsProfileId()
{
	$effectsProfileId = 'telephony-class-application';

	return $effectsProfileId;
}

function get_gender_voice()
{
	if (isset($_GET['voice'])) {
		$get_voice = strtoupper($_GET['voice']);
		if (($get_voice == 'MALE') || $get_voice == 'FEMALE') {
			$gender_voice = $get_voice;
		} else {
			die('Please use voice value from 1."MALE", 2."FEMALE" or check the spelling');
		}
	} else {
		$gender_voice = 'FEMALE';
	}
	return $gender_voice;
}

function get_language()
{
	if (isset($_GET['language'])) {
		return trim($_GET['language']);
	}
	return 'en-IN';
}

?>