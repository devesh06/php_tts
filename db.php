<?php

class Db
{
    public $db;
    public function __construct()
    {
        $config_path = '/var/config/tts/config.json';
        if (!file_exists($config_path)) {
            die("The file $config_path does not exists");

        }
        $this->config = json_decode(file_get_contents($config_path), True);
    }

    public function get_conn()
    {
        try {
            if (!isset($this->db)) {
                $this->db = new PDO("mysql:host=".$this->config['database']['host'].";
                    dbname=".$this->config['database']['db'], $this->config['database']['username'],
                    $this->config['database']['password']);
                if (!$this->db) {
                    die("could not connect to the database");
                }
            }
            return $this->db;
        } catch(PDOException $e) {
            echo($e->getMessage());
        }
    }

    public function execute_query($query)
    {
        try {
            $stmt = $this->get_conn()->prepare($query);
            if ($stmt->execute()) {
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
            } else {
                print_r($stmt->errorInfo());
                die;
            }
            return $row;
        } catch(Exception $e) {
            $log_msg = "Error in executing query".$query." ".$e->getMessage();
            file_put_contents($this->new_db->log_path,$log_msg,FILE_APPEND);
            die($log_msg);
        }
    }
}

?>
